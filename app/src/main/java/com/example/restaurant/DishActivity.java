package com.example.restaurant;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class DishActivity extends AppCompatActivity {

    TextView textView;
    Button button;
    Spinner spinner;
    RadioGroup radioGroup;
    RadioButton radioButtonVisa;
    RadioButton radioButtonMastercard;
    EditText editTextName, editTextDir, editTextCard;
    String card;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish);

        textView = findViewById(R.id.selected_dish);
        spinner = findViewById(R.id.spinner);
        editTextName = findViewById(R.id.edit_name);
        editTextDir = findViewById(R.id.edit_direccion);
        editTextCard = findViewById(R.id.edit_credit_card_number);
        radioGroup = findViewById(R.id.radio_group_cards);
        radioButtonVisa = findViewById(R.id.visa);
        radioButtonMastercard = findViewById(R.id.mastercard);
        button = findViewById(R.id.button);

        Intent intent = getIntent();
        String dish = intent.getStringExtra("DISH");
        textView.setText(dish);

        Integer[] numberdish = new Integer[]{1,2,3,4,5,6,7,8,9,10};
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item, numberdish);
        spinner.setAdapter(adapter);

        Resources res = getResources();

        button.setOnClickListener(v -> {
            String name = editTextName.getText().toString();
            String dir = editTextDir.getText().toString();
            String cardn = editTextCard.getText().toString();
            String dishn = spinner.getSelectedItem().toString();

       //     if (name.equals("") || dir.equals("") || cardn.equals("")) {

                if (radioButtonVisa.isChecked()) {
                    card = (String) getText(R.string.visa);
                    AlertDialog.Builder builder = new AlertDialog.Builder(DishActivity.this);
                    builder.setTitle(R.string.dialog_title);
                    builder.setCancelable(true);
                    builder.setMessage(String.format(res.getString(R.string.succesfull), name , dir, dishn, card, cardn));
                    builder.setPositiveButton("Si", (dialog, which) -> {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    });
                    builder.setNegativeButton("NO", (dialog, which) -> {
                    });
                    builder.create().show();
                }
                if (radioButtonMastercard.isChecked()) {
                    card = (String) getText(R.string.visa);
                    AlertDialog.Builder builder = new AlertDialog.Builder(DishActivity.this);
                    builder.setTitle(R.string.dialog_title);
                    builder.setCancelable(true);
                    builder.setMessage(String.format(res.getString(R.string.succesfull), name , dir, dishn, card, cardn));
                    builder.setPositiveButton("Si", (dialog, which) -> {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    });
                    builder.setNegativeButton("NO", (dialog, which) -> {
                    });
                    builder.create().show();
                }

      //      }

      /*      else {
                Snackbar.make(linearLayout, R.string.msg_snack_bar, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_snack_bar_text, v1 -> {
                }).show();
            }*/
        });

    }
}