package com.example.restaurant;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<String> text2 = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.grid_view_dishes);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, text, image, text2);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            String dish = text2.get(position);
            Intent intent = new Intent(MainActivity.this, DishActivity.class);
            intent.putExtra("DISH", dish);
            startActivity(intent);
        });

    }

    private void fillArray() {
        text.add("Plato 1");
        text.add("Plato 2");
        text.add("Plato 3");
        text.add("Plato 4");
        text.add("Plato 5");
        text.add("Plato 6");
        text.add("Plato 7");
        text.add("Plato 8");

        image.add(R.drawable.ajidegallina);
        image.add(R.drawable.arrozconpollo);
        image.add(R.drawable.carapulcra);
        image.add(R.drawable.ceviche);
        image.add(R.drawable.lomosaltado);
        image.add(R.drawable.pachamanca);
        image.add(R.drawable.rocotorelleno);
        image.add(R.drawable.sopaseca);

        text2.add("Aji de Gallina");
        text2.add("Arroz con Pollo");
        text2.add("Carapulcra");
        text2.add("Ceviche");
        text2.add("Lomo Saltado");
        text2.add("Pachamanca");
        text2.add("Rocoto Relleno");
        text2.add("Sopa Seca");
    }
}